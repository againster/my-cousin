package mq

import (
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"time"
	"fmt"
	"sync"
)

// kafka conf
type KafkaReaderOptions struct {
	Server         string   `ini:"server"`
	Topic          []string `ini:"topic"`
	GroupId        string   `ini:"group_id"`
	Offset         string   `ini:"offset"`
	PollTimeout    int      `ini:"poll_timeout"`
	SessionTimeout int      `ini:"session_timeout"`
	AutoCommit     bool     `ini:"auto_commit"`
}

type KafkaWriterOptions struct {
	Server string `ini:"server"`
	Topic  string `ini:"topic"`
}

// kafka
type KafkaReaderWriter struct {
	Reader *KafkaReader
	Writer *KafkaWriter
}

type KafkaReader struct {
	conf           *KafkaReaderOptions
	consumer       *kafka.Consumer
	enableConsumer bool
}

type KafkaWriter struct {
	conf                 *KafkaWriterOptions
	producer             *kafka.Producer
	producerChan         chan kafka.Event
	producerAsyncChan    chan kafka.Event
	producerAsyncChanLen int
	enableProducer       bool
}

// init producer and consumer
func NewKafkaReaderWriter(r KafkaReaderOptions,
	w KafkaWriterOptions) (*KafkaReaderWriter, error) {
	var err error
	mq := &KafkaReaderWriter{}
	mq.Writer, err = NewKafkaWriter(w)
	if err != nil {
		return nil, err
	}

	mq.Reader, err = NewKafkaReader(r)
	if err != nil {
		defer mq.Writer.Close()
		return nil, err
	}
	return mq, nil
}

// init consumer
func NewKafkaReader(option KafkaReaderOptions) (*KafkaReader, error) {
	var err error
	reader := &KafkaReader{
		conf:           &option,
		enableConsumer: true,
	}
	// init consumer
	reader.consumer, err = kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":  option.Server,
		"group.id":           option.GroupId,
		"session.timeout.ms": option.SessionTimeout,
		"default.topic.config": kafka.ConfigMap{
			"auto.offset.reset": option.Offset,
		},
		"enable.auto.commit": option.AutoCommit,
	})
	if err != nil {
		return nil, err
	}

	err = reader.consumer.SubscribeTopics(option.Topic, nil)
	if err != nil {
		return nil, err
	}

	return reader, nil
}

// init producer
func NewKafkaWriter(option KafkaWriterOptions) (*KafkaWriter, error) {
	var err error
	const producerAsyncChanLen = 1000
	writer := &KafkaWriter{
		conf:                 &option,
		producerAsyncChanLen: producerAsyncChanLen,
		producerAsyncChan:    make(chan kafka.Event, producerAsyncChanLen),
		producerChan:         make(chan kafka.Event),
	}

	writer.producer, err = kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": option.Server,
	})
	if err != nil {
		return nil, err
	}

	return writer, nil
}

func (mq *KafkaReader) Read(p []byte) (int, error) {
	readTimeout := time.Duration(mq.conf.PollTimeout)
	message, err := mq.consumer.ReadMessage(readTimeout)
	if err != nil {
		return 0, err
	}
	p = message.Value
	return len(p), nil
}

func (mq *KafkaReader) PollTo(f func(*kafka.Consumer, *kafka.Message) error) {
	// loop to subscribe
	for mq.enableConsumer == true {
		event := mq.consumer.Poll(mq.conf.PollTimeout)
		if event != nil {
			switch e := event.(type) {
			case *kafka.Message:
				f(mq.consumer, e)
			case kafka.PartitionEOF:
			case kafka.Error:
			default:
			}
		}
	}
}

func (mq *KafkaWriter) Write(topic string, payload []byte) (int, error) {
	// init message
	message := &kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic:     &topic,
			Partition: kafka.PartitionAny,
		},
		Value: payload,
	}

	// send message
	err := mq.producer.Produce(message, mq.producerChan)
	if err != nil {
		return 0, err
	}

	result := <-mq.producerChan
	reply := result.(*kafka.Message)
	if reply.TopicPartition.Error != nil {
		return 0, reply.TopicPartition.Error
	}

	return len(payload), nil
}

func (mq *KafkaWriter) AsyncWrite(topic string, payload []byte) (int, error) {
	// init message
	message := &kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic:     &topic,
			Partition: kafka.PartitionAny,
		},
		Value: payload,
	}

	// send message
	err := mq.producer.Produce(message, mq.producerAsyncChan)
	if err != nil {
		return 0, err
	}

	return len(payload), nil
}

func (mq *KafkaWriter) AsyncReplyTo(f func(topic *string, payload []byte,
	err error)) {
	if f == nil {
		f = func(*string, []byte, error) {}
	}
	// check send result
	for mq.enableProducer == true {
		select {
		case reply := <-mq.producerAsyncChan:
			message := reply.(*kafka.Message)
			f(message.TopicPartition.Topic, message.Value,
				message.TopicPartition.Error)
		}
	}
}

func (mq *KafkaReaderWriter) Close() error {
	if err := mq.Writer.Close(); err != nil {
		return err
	}
	return mq.Reader.Close()
}

func (mq *KafkaWriter) Close() error {
	if mq.producer != nil {
		mq.enableProducer = false
		close(mq.producerChan)
		close(mq.producerAsyncChan)
		mq.producer.Close()
	}
	mq.producer = nil
	mq.producerChan = nil
	mq.producerAsyncChan = nil
	return nil
}

func (mq *KafkaReader) Close() error {
	if mq.consumer != nil {
		mq.enableConsumer = false
		err := mq.consumer.Close()
		if err != nil {
			return err
		}
	}
	mq.consumer = nil
	return nil
}

////////////////////////////////////////////////////////////////////////////////

type messageWrapper struct {
	message  *kafka.Message
	consumer *kafka.Consumer
}

type KafkaReaderPolicy interface {
	worker(chan *messageWrapper, int)
}

type KafkaReaderWorker struct {
	policyPipe chan *messageWrapper
	worker     KafkaReaderPolicy
}

// init routine
func NewKafkaReaderWorker(pipeCapacity, workerCount int,
	policy KafkaReaderPolicy) *KafkaReaderWorker {
	p := &KafkaReaderWorker{
		policyPipe: make(chan *messageWrapper, pipeCapacity),
		worker:     policy,
	}

	// send routines, you can add another one
	for i := 0; i < workerCount; i++ {
		go p.worker.worker(p.policyPipe, i)
	}
	return p
}

// callback handler
func (r *KafkaReaderWorker) Handler(c *kafka.Consumer,
	message *kafka.Message) error {
	wrapper := &messageWrapper{
		message,
		c,
	}
	r.policyPipe <- wrapper // send worker handles cpu intensive activity
	return nil
}

type KafkaReaderPolicyBatchBuffer struct {
	workerCtx   *kafkaReaderPolicyBatchBufferCtx
	tpMaxOffset sync.Map // sharing max offset of topic partition
}

type kafkaReaderPolicyBatchBufferCtx struct {
	n                 int
	reader            *KafkaReader
	msg               *kafka.Message
	maxLenMsgBuf      int
	timeoutMsgBuf     int
	msgBuf            map[string][][]byte
	msgCaller         func(string, [][]byte) error
	consumer          *kafka.Consumer
	commitTP          []kafka.TopicPartition
	pauseTP           []kafka.TopicPartition
	pauseConsuming    bool
	commitCustomOk    bool
	commitOffsetOk    bool
	firstCommitPolicy FirstCommitPolicy
}

const (
	FirstCommitPolicyOffset  = "offset"
	FirstCommitPolicyMessage = "message"
)

type FirstCommitPolicy string

func NewKafkaReaderPolicyBatchBuffer(maxLenMsgBuf, timeoutMsgBuf int,
	firstCommitPolicy FirstCommitPolicy, reader *KafkaReader, caller func(string,
		[][]byte) error) (*KafkaReaderPolicyBatchBuffer, error) {

	ctx := &kafkaReaderPolicyBatchBufferCtx{
		reader:            reader,
		maxLenMsgBuf:      maxLenMsgBuf,
		timeoutMsgBuf:     timeoutMsgBuf,
		firstCommitPolicy: firstCommitPolicy,
		msgCaller:         caller,
		commitTP:          make([]kafka.TopicPartition, 0, maxLenMsgBuf),
	}

	for _, v := range ctx.reader.conf.Topic {
		tp := kafka.TopicPartition{
			Topic: &v, Partition: kafka.PartitionAny,
		}
		ctx.pauseTP = append(ctx.pauseTP, tp)
	}

	p := &KafkaReaderPolicyBatchBuffer{
		workerCtx: ctx,
	}
	return p, nil
}

// send thread handler
func (p *KafkaReaderPolicyBatchBuffer) worker(
	policyPipe chan *messageWrapper, n int) {
	ctx := &kafkaReaderPolicyBatchBufferCtx{
		n:                 n,
		reader:            p.workerCtx.reader,
		msgBuf:            make(map[string][][]byte),
		maxLenMsgBuf:      p.workerCtx.maxLenMsgBuf,
		timeoutMsgBuf:     p.workerCtx.timeoutMsgBuf,
		firstCommitPolicy: p.workerCtx.firstCommitPolicy,
		msgCaller:         p.workerCtx.msgCaller,
		commitCustomOk:    true,
	}
	ticker := time.NewTicker(time.Duration(ctx.timeoutMsgBuf) *
		time.Millisecond) // set timer
	for {
		select {
		case x := <-policyPipe: // failed msg will be cached in list
			ctx.msg = x.message
			ctx.consumer = x.consumer
			p.bufferOrHandleMessage(ctx)
		case <-ticker.C: // every KResendInterval seconds resent msg
			p.handleMessage(ctx)
		}
	}
}

func (p *KafkaReaderPolicyBatchBuffer) bufferOrHandleMessage(
	ctx *kafkaReaderPolicyBatchBufferCtx) {
	p.bufferMessage(ctx)

	// no. of msgBuf greater than threshold indicates it's time to trigger
	// handler all messages.
	if len(ctx.commitTP) < ctx.maxLenMsgBuf {
		return
	}

	p.handleMessage(ctx)
}

// BufferMessage buffers a message to list,
// return value true indicates buffering ok; false shows buffering failed,
// you should insert message to mysql
func (p *KafkaReaderPolicyBatchBuffer) bufferMessage(
	ctx *kafkaReaderPolicyBatchBufferCtx) {
	if ctx.msg == nil {
		return
	}

	recentOffset := int64(ctx.msg.TopicPartition.Offset)
	partition := ctx.msg.TopicPartition.Partition
	topic := ctx.msg.TopicPartition.Topic

	tpKey := fmt.Sprintf("%s:%d", topic, partition)
	lastOffset, ok := p.tpMaxOffset.Load(tpKey)
	if !ok || lastOffset.(int64) < recentOffset {
		p.tpMaxOffset.Store(tpKey, recentOffset)

		buf, ok := ctx.msgBuf[*topic]
		if !ok {
			buf = make([][]byte, 0, ctx.maxLenMsgBuf)
		}
		buf = append(buf, ctx.msg.Value)
		ctx.msgBuf[*topic] = buf

		ctx.commitTP = append(ctx.commitTP, ctx.msg.TopicPartition)
		ctx.commitTP[len(ctx.commitTP)-1].Offset++
	} else {
		// todo, log replication message
	}
}

// handle all messages
func (p *KafkaReaderPolicyBatchBuffer) handleMessage(
	ctx *kafkaReaderPolicyBatchBufferCtx) {
	if ctx.commitTP == nil || len(ctx.commitTP) == 0 {
		return
	}
	switch string(ctx.firstCommitPolicy) {
	case FirstCommitPolicyMessage:
		p.commitMessageFirst(ctx)
		p.commitOverHandler(ctx, ctx.commitCustomOk)
	case FirstCommitPolicyOffset:
		p.commitOffsetFirst(ctx)
		p.commitOverHandler(ctx, ctx.commitOffsetOk)
	}

}

func (p *KafkaReaderPolicyBatchBuffer) commitMessageFirst(
	ctx *kafkaReaderPolicyBatchBufferCtx) {
	// call handler to handle all message
	for k, v := range ctx.msgBuf {
		if err := ctx.msgCaller(k, v); err != nil {
			ctx.commitCustomOk = false
			return
		}
	}
	ctx.commitCustomOk = true
	// if custom side work ok, ack all messages
	_, err := ctx.consumer.CommitOffsets(ctx.commitTP)
	if err != nil {
		ctx.commitOffsetOk = false
		// todo, log commit offset error
		return
	}
	ctx.commitOffsetOk = true

	return
}

func (p *KafkaReaderPolicyBatchBuffer) commitOffsetFirst(
	ctx *kafkaReaderPolicyBatchBufferCtx) {
	// ack all messages
	_, err := ctx.consumer.CommitOffsets(ctx.commitTP)
	if err != nil {
		ctx.commitOffsetOk = false
		// todo, log commit offset error
		return
	}
	ctx.commitOffsetOk = true

	// call handler to handle all message
	for k, v := range ctx.msgBuf {
		if err := ctx.msgCaller(k, v); err != nil {
			ctx.commitCustomOk = false
			return
		}
	}
	ctx.commitCustomOk = true
	return
}

func (p *KafkaReaderPolicyBatchBuffer) commitOverHandler(
	ctx *kafkaReaderPolicyBatchBufferCtx, ok bool) {
	if !ok {
		if ctx.pauseConsuming == false {
			err := ctx.consumer.Pause(ctx.pauseTP)
			if err == nil {
				ctx.pauseConsuming = true
			} else {
				// todo, log pause consuming error
			}
		}
		return
	}

	// reset buffer and time for the next buffering
	ctx.commitTP = make([]kafka.TopicPartition, 0, ctx.maxLenMsgBuf)
	ctx.msgBuf = make(map[string][][]byte)

	// if pause consuming, we resume it
	if ctx.pauseConsuming == true {
		err := ctx.consumer.Resume(ctx.pauseTP)
		if err == nil {
			ctx.pauseConsuming = false
		} else {
			// todo, log resume consuming error
		}
	}
}
