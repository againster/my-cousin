Name:           es-email-sender
Version:        1.0.0
Release:        1%{?dist}
Summary:        Eshine email sender

License:        GPLv3
URL:            www.eshine.com
#Source0:       www.eshine.com/source/test0.tar.gz

#BuildRequires: gcc
#Requires:       vim
BuildArch:      x86_64

#%package
%description
eshine email sender. we change the world.
%prep
%build
%install
%es_install %{moduledir}/rootfs
%es_filelist %{moduledir}/rootfs
%files -f %filelist
%clean
%es_clean
%changelog

%pre
%es_usergroupadd eshine eshine
%post
%es_systemd_post es-email-sender
%preun
%systemd_preun es-email-sender
%postun
%systemd_postun es-email-sender
