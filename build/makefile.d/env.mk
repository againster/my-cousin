# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUGHOR: ArrowLee
# DATE: 20190923

abs_top_dir = ${abs_top_dird}

abs_top_3partydir = ${abs_top_dir}/3party
abs_top_builddir = ${abs_top_dir}/build
abs_top_deploydir = ${abs_top_dir}/deploy
abs_top_mandir = ${abs_top_dir}/man
abs_top_srcdir = ${abs_top_dir}/src

abs_top_build_pkgdir = ${abs_top_builddir}/pkg
abs_top_build_depdir = ${abs_top_builddir}/dep
abs_top_build_makefiledir = ${abs_top_builddir}/makefile.d

abs_top_deploy_pkgrpmdir = ${abs_top_deploydir}/pkgrpm
abs_top_deploy_pkgdebdir = ${abs_top_deploydir}/pkgdeb
abs_top_deploy_pkgtardir = ${abs_top_deploydir}/pkgtar

abs_top_src_vardir = ${abs_top_srcdir}/var
abs_top_src_libprotodir = ${abs_top_srcdir}/libproto

env = ${envd}
envvardir = ${abs_top_src_vardir}/${env}

abs_builddir = $(realpath .)
abs_srcdir = $(realpath .)
builddir = .
srcdir = .

top_build_prefix =
top_builddir =
