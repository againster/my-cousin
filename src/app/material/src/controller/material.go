package controller

import (
	. "conf"
	"fmt"
	"github.com/gin-gonic/gin"
	"model"
	"model/mysql"
	"net/http"
)

type Material struct{}

func NewMaterial() *Material {
	return &Material{}
}

type commonArg struct {
	Id           int64   `json:"id"`
	Ctime        int64   `json:"ctime"`
	Name         string  `json:"name"`
	UnitPrice    float64 `json:"unit_price"`
	ProjName     string  `json:"proj_name"`
	Model        string  `json:"model"`
	TotalPrice   float64 `json:"total_price"`
	MaxPrice     float64 `json:"max_price"`
	MinPrice     float64 `json:"min_price"`
	StoreType    string  `json:"store_type"`
	MaterialType string  `json:"material_type"`
	Buyer        string  `json:"buyer"`
	Keeper       string  `json:"keeper"`
	StoreNo      string  `json:"store_no"`
	MeasureUnit  string  `json:"measure_unit"`
	Inspector    string  `json:"inspector"`
	Supplier     string  `json:"supplier"`
	MaterialNo   string  `json:"material_no"`
	Total        float64 `json:"total"`
	Remaining    float64 `json:"remaining"`
	Comment      string  `json:"comment"`
}

func common2Mysql(a *commonArg, m *model.Material) {
	m.Id = a.Id
	m.Ctime = a.Ctime
	m.Name = a.Name
	m.UnitPrice = a.UnitPrice
	m.ProjName = a.ProjName
	m.Model = a.Model
	m.TotalPrice = a.TotalPrice
	m.MaxPrice = a.MaxPrice
	m.MinPrice = a.MinPrice
	m.StoreType = a.StoreType
	m.MaterialType = a.MaterialType
	m.Buyer = a.Buyer
	m.Keeper = a.Keeper
	m.StoreNo = a.StoreNo
	m.MeasureUnit = a.MeasureUnit
	m.Inspector = a.Inspector
	m.Supplier = a.Supplier
	m.MaterialNo = a.MaterialNo
	m.Total = a.Total
	m.Remaining = a.Remaining
	m.Comment = a.Comment
}

func mysql2Common(a *model.Material, m *commonArg) {
	m.Id = a.Id
	m.Ctime = a.Ctime
	m.Name = a.Name
	m.UnitPrice = a.UnitPrice
	m.ProjName = a.ProjName
	m.Model = a.Model
	m.TotalPrice = a.TotalPrice
	m.MaxPrice = a.MaxPrice
	m.MinPrice = a.MinPrice
	m.StoreType = a.StoreType
	m.MaterialType = a.MaterialType
	m.Buyer = a.Buyer
	m.Keeper = a.Keeper
	m.StoreNo = a.StoreNo
	m.MeasureUnit = a.MeasureUnit
	m.Inspector = a.Inspector
	m.Supplier = a.Supplier
	m.MaterialNo = a.MaterialNo
	m.Total = a.Total
	m.Remaining = a.Remaining
	m.Comment = a.Comment
}

type newArg struct {
	commonArg
}

type updateArg struct {
	commonArg
}

type delArg struct {
	commonArg
}

func (m *Material) New(ctx *gin.Context) {
	arg := &newArg{}
	ret := &Ret{}
	defer func(ret *Ret) {
		ctx.JSON(http.StatusOK, ret)
	}(ret)
	ctx.ShouldBindJSON(arg)
	mod := model.Material{}
	common2Mysql(&arg.commonArg, &mod)
	if err := mysql.NewDB().Create(&mod).Error; err != nil {
		Logger.Error(err.Error())
		ret.Desc = "数据库错误"
		ret.Code = 1000
		return
	}
	ret.Desc = "成功"
}

func (m *Material) Update(ctx *gin.Context) {
	arg := &updateArg{}
	ret := &Ret{}
	defer func(ret *Ret) {
		ctx.JSON(http.StatusOK, ret)
	}(ret)
	ctx.ShouldBindJSON(arg)
	mod := model.Material{}
	common2Mysql(&arg.commonArg, &mod)
	fmt.Println("hello fuck:", mod)
	if err := mysql.NewDB().Save(&mod).Error; err != nil {
		Logger.Error(err.Error())
		ret.Desc = "数据库错误"
		ret.Code = 1000
		return
	}
	ret.Desc = "成功"
}

func (m *Material) Read(ctx *gin.Context) {
	ret := &Ret{}
	defer func(ret *Ret) {
		ctx.JSON(http.StatusOK, ret)
	}(ret)
	mod := new([]*model.Material)
	if err := mysql.NewDB().Find(mod).Error; err != nil {
		Logger.Error(err.Error())
		ret.Desc = "数据库错误"
		ret.Code = 1000
		return
	}
	ret.Desc = "成功"
	a := make([]*commonArg, 0, len(*mod))
	for _, v := range *mod {
		arg := commonArg{}
		mysql2Common(v, &arg)
		a = append(a, &arg)
	}
	ret.Data = a
}

func (m *Material) Delete(ctx *gin.Context) {
	var arg []delArg
	ret := &Ret{}
	defer func(ret *Ret) {
		ctx.JSON(http.StatusOK, ret)
	}(ret)
	ctx.ShouldBindJSON(&arg)
	del := make([]int64, 0, len(arg))
	for _, v := range arg {
		del = append(del, v.Id)
	}
	err := mysql.NewDB().Delete(model.Material{}, "id IN(?)", del).Error
	if err != nil {
		Logger.Error(err.Error())
		ret.Desc = "数据库错误"
		ret.Code = 1000
		return
	}
	ret.Desc = "成功"
}
