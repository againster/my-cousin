package util

import (
	"encoding/json"
	"fmt"
	"os"
	"runtime"
	"go.uber.org/zap"
	"io/ioutil"
)

func Clone(src interface{}, dst interface{}) error {
	if dst == nil {
		return fmt.Errorf("dst cannot be nil")
	}
	if src == nil {
		return fmt.Errorf("src cannot be nil")
	}
	bytes, err := json.Marshal(src)
	if err != nil {
		return fmt.Errorf("Unable to marshal src: %s", err)
	}
	err = json.Unmarshal(bytes, dst)
	if err != nil {
		return fmt.Errorf("Unable to unmarshal into dst: %s", err)
	}
	return nil
}

func Exists(filename string) bool {
	if _, err := os.Stat(filename); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func Catch(logger *zap.Logger) {
	rec := recover()
	if rec == nil {
		return
	}
	location := rec.(error).Error() + ": "
	pc := make([]uintptr, 10)
	n := runtime.Callers(0, pc)
	if n != 0 {
		pc = pc[:n]
		frames := runtime.CallersFrames(pc)
		for {
			frame, more := frames.Next()
			location = fmt.Sprintf("%s %s:%d:%s", location, frame.File,
				frame.Line, frame.Function)
			if !more {
				break
			}
		}
	}
	logger.Error("catch a panic", zap.String("stack trace", location))
}

func WritePidFile(pidfile string) error {
	if pidfile == "" {
		return fmt.Errorf("error pidfile %s", pidfile)
	}

	err := ioutil.WriteFile(pidfile, []byte(fmt.Sprintf("%d", os.Getpid())),
		os.FileMode(0644))
	if err != nil {
		return fmt.Errorf("error write pidfile %s", err)
	}

	return nil
}