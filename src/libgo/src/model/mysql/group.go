package mysql

const (
	KDefaultGroupName = "eshine"
)

type Group struct {
	Gid          int64  `gorm:"column:gid"`
	GroupName    string `gorm:"column:group_name;primary_key;"`
	OfficialName string `gorm:"column:official_name;"`
}

func NewGroup() *Group {
	return &Group{}
}

func (g *Group) TableName() string {
	return "group"
}
