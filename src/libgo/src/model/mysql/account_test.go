package mysql

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"time"
)

const SqlUrl = `es:jN9/2yJBf.wk%jO&@tcp(10.118.0.17:3306)/es?charset=utf8&parseTime=True&timeout=3000ms&readTimeout=3000ms&writeTimeout=3000ms`

func Test_account(t *testing.T) {
	Convey("account read", t, func() {
		db, err := gorm.Open("mysql", SqlUrl)
		So(err, ShouldBeNil)
		a := Account{
			Username: "lz",
			Password: "password",
			Phone:    "18899772155",
			Email:    "lissa5@163.com",
			Security_q_1: "n",
			Security_a_1: "n",
			Security_q_2: "p",
			Security_a_2: "p",
			Security_q_3: "q",
			Security_a_3: "q",
			Ctime: time.Unix(0, 0),
		}
		err = a.Insert(db)
		So(err, ShouldBeNil)
	})
}
