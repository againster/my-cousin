package mysql

import (
	"time"
	"database/sql"
)

/*
-- create table
CREATE TABLE IF NOT EXISTS account (
    `uid` BIGINT AUTO_INCREMENT COMMENT 'user id',
    `rid` BIGINT DEFAULT 1 COMMENT 'role id. privilege controller',
    `username` VARCHAR(128) NOT NULL COMMENT 'username. whatever plain',
    `password` VARCHAR(1024) NOT NULL COMMENT 'hashed password. consists of $way$salt$hash(p)',
    `phone` VARCHAR(512) DEFAULT '' COMMENT 'phone. consists of $way$symmetrically_encrypt(+country_code no). this column only exists one way.',
    `email` VARCHAR(512) DEFAULT '' COMMENT 'email. consists of $way$symmetrically_encrypt(e). this column only exists one way.',
    `otp` VARCHAR(512) DEFAULT '' COMMENT 'secret of hotp or totp algorithm. consists of $way$secret',
    `security_q_1` VARCHAR(1024) DEFAULT '' COMMENT 'security question. consists of $base64(q)$way$salt$hash(a)',
    `security_q_2` VARCHAR(1024) DEFAULT '' COMMENT 'security question. consists of $base64(q)$way$salt$hash(a)',
    `security_q_3` VARCHAR(1024) DEFAULT '' COMMENT 'security question. consists of $base64(q)$way$salt$hash(a)',
    `ctime` DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT 'creation time, read only column.',
    `mtime` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modified time, read only column. when update a row, it should be a condition',
    --
    CONSTRAINT pk PRIMARY KEY (`username`),
    CONSTRAINT uniq_uid UNIQUE KEY (`uid`)
)  ENGINE=INNODB AUTO_INCREMENT=1;
 */

type Account struct {
	Uid          int64          `gorm:"column:uid;AUTO_INCREMENT;unique_index;"`
	Rid          int64          `gorm:"column:rid"`
	Gid          int64          `gorm:"column:gid"`
	Username     string         `gorm:"column:username;primary_key;"`
	Password     string         `gorm:"column:password"`
	Phone        sql.NullString `gorm:"column:phone;DEFAULT NULL;"`
	Email        sql.NullString `gorm:"column:email;DEFAULT NULL;"`
	Otp          sql.NullString `gorm:"column:otp;DEFAULT NULL;"`
	Security_1   sql.NullString `gorm:"column:security_1;DEFAULT NULL;"`
	Security_q_1 string         `gorm:"-"`
	Security_a_1 string         `gorm:"-"`
	Security_2   sql.NullString `gorm:"column:security_2;DEFAULT NULL;"`
	Security_q_2 string         `gorm:"-"`
	Security_a_2 string         `gorm:"-"`
	Security_3   sql.NullString `gorm:"column:security_3;DEFAULT NULL;"`
	Security_q_3 string         `gorm:"-"`
	Security_a_3 string         `gorm:"-"`
	Ctime        time.Time      `gorm:"column:ctime;default:CURRENT_TIMESTAMP"`
	Mtime        time.Time      `gorm:"column:mtime;default:CURRENT_TIMESTAMP"`
}

func (t *Account) TableName() string { return "account" }

func NewAccount() *Account {
	return &Account{
		Rid: KDefaultRoleId,
		Gid: KDefaultGroupId,
	}
}

const (
	KDefaultGroupId   = int64(1)
	KDefaultRoleId    = int64(1)
)

