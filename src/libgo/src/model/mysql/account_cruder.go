package mysql

import (
	"github.com/jinzhu/gorm"
	"database/sql"
)

type AccountCruder struct{}

func NewAccountCruder() *AccountCruder {
	return &AccountCruder{}
}

/*----------------------------------------------------------------------------*/

// Insert inserts a row to db. Uid, Ctime, Mtime will be ignore.
func (p *AccountCruder) Insert(db *gorm.DB, c *Account) error {
	helper := NewAccountHelper()
	password, err := helper.HashPassword(c.Password)
	if err != nil {
		return err
	}

	var security_1, security_2, security_3 sql.NullString
	if c.Security_q_1 != "" && c.Security_a_1 != "" {
		security_1.String, err = helper.EncodeQuestionAnswer(c.Security_q_1,
			c.Security_a_1)
		if err != nil {
			return err
		}
		security_1.Valid = true
	}
	if c.Security_q_2 != "" && c.Security_a_2 != "" {
		security_2.String, err = helper.EncodeQuestionAnswer(c.Security_q_2,
			c.Security_a_2)
		if err != nil {
			return err
		}
		security_2.Valid = true
	}
	if c.Security_q_3 != "" && c.Security_a_3 != "" {
		security_3.String, err = helper.EncodeQuestionAnswer(c.Security_q_3,
			c.Security_a_3)
		if err != nil {
			return err
		}
		security_3.Valid = true
	}
	n := &Account{
		Rid:        c.Rid,
		Gid:        c.Gid,
		Username:   c.Username,
		Password:   password,
		Phone:      c.Phone,
		Email:      c.Email,
		Otp:        c.Otp,
		Security_1: security_1,
		Security_2: security_2,
		Security_3: security_3,
	}
	return p.insert(db, n)
}

func (p *AccountCruder) insert(db *gorm.DB, c *Account) error {
	db = db.Create(&c)
	return db.Error
}

/*----------------------------------------------------------------------------*/
func (p *AccountCruder) QueryByUsernameGroupName(db *gorm.DB,
	username, groupName string) (*Account, *Group, error) {
	if groupName == "" {
		groupName = KDefaultGroupName
	}
	group, err := NewGroupCruder().QueryByGroupName(db, groupName)
	if err != nil {
		return nil, nil, err
	}

	account, err := p.QueryByUsernameGid(db, username, group.Gid)
	if err != nil {
		return nil, nil, err
	}
	return account, group, nil
}

func (p *AccountCruder) QueryByUsername(db *gorm.DB, name string) (*Account,
	error) {
	return p.QueryByUsernameGid(db, name, KDefaultGroupId)
}

func (p *AccountCruder) QueryByUid(db *gorm.DB, uid int64) (*Account, error) {
	var a Account
	db.Where("uid = ?", uid).Find(&a)
	if db.RecordNotFound() {
		return nil, gorm.ErrRecordNotFound
	}
	return &a, db.Error
}

func (p *AccountCruder) QueryByUsernameGid(db *gorm.DB, name string,
	gid int64) (*Account, error) {
	var a Account
	db.Where("username = ? and gid = ?", name, gid).Find(&a)
	if db.RecordNotFound() {
		return nil, gorm.ErrRecordNotFound
	}
	return &a, db.Error
}

func (p *AccountCruder) QueryByPhone(db *gorm.DB, phone string) ([]*Account,
	error) {
	var a []*Account
	db.Where("phone = ?", phone).Find(&a)
	return a, db.Error
}

func (p *AccountCruder) QueryByEmail(db *gorm.DB, email string) ([]*Account,
	error) {
	var a []*Account
	db.Where("email = ?", email).Find(&a)
	return a, db.Error
}

/*----------------------------------------------------------------------------*/
func (p *AccountCruder) UpdatePhoneByUsernameGid(db *gorm.DB, username string,
	phone string, gid int64) error {
	encPhone, err := NewAccountHelper().EncryptPhone(phone)
	if err != nil {
		return err
	}
	db.Where("username = ? and gid = ?", username, gid).Update("phone",
		encPhone)
	return db.Error
}

func (p *AccountCruder) UpdateByUsernameGid(db *gorm.DB, username string,
	gid int64, changedWhat uint64, columnValue ...interface{}) error {
	return db.Error
}

/*----------------------------------------------------------------------------*/
func (p *AccountCruder) DeleteByUid(db *gorm.DB, uid int64) (error) {
	db.Where("uid = ?", uid).Delete(Account{})
	return db.Error
}

func (p *AccountCruder) DeleteByUsername(db *gorm.DB, username string) (error) {
	p.DeleteByUsernameGid(db, username, KDefaultGroupId)
	return db.Error
}

func (p *AccountCruder) DeleteByUsernameGid(db *gorm.DB, username string,
	gid int64) (error) {
	db.Where("username = ? and gid = ?", username,
		gid).Delete(Account{})
	return db.Error
}
