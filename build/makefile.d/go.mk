# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUGHOR: ArrowLee
# DATE: 20180726

include env.mk
include com.mk

abs_moduledir = $(realpath ./..)
modulename = $(notdir ${abs_moduledir})

pkgdatadir = $(datadir)/eshine
pkgincludedir = $(includedir)/eshine
pkglibdir = $(libdir)/eshine
pkglibexecdir = $(libexecdir)/eshine

prefix = $(shell (test -z ${prefixd} && echo "/usr/local") || echo "${prefixd}")
bindir = ${exec_prefix}/bin
datadir = ${datarootdir}
datarootdir = ${prefix}/share
docdir = ${datarootdir}/doc/${PACKAGE_TARNAME}
dvidir = ${docdir}
exec_prefix = ${prefix}
htmldir = ${docdir}
includedir = ${prefix}/include
infodir = ${datarootdir}/info
libdir = ${exec_prefix}/lib
libexecdir = ${exec_prefix}/libexec
localedir = ${datarootdir}/locale
localstatedir = ${prefix}/var
mandir = ${datarootdir}/man
oldincludedir = /usr/include
pdfdir = ${docdir}
psdir = ${docdir}
sbindir = ${exec_prefix}/sbin
sharedstatedir = ${prefix}/com
sysconfdir = ${prefix}/etc
unitdir = ${libdir}/systemd/system
userunitdir = ${libdir}/systemd/user

build = x86_64-unknown-linux-gnu
build_alias = 
build_cpu = x86_64
build_os = linux-gnu
build_vendor = unknown
host = x86_64-unknown-linux-gnu
host_alias = 
host_cpu = x86_64
host_os = linux-gnu
host_vendor = unknown

transform = $(program_transform_name)

program_transform_name = s,x,x,
target_alias =

LIBTOOL = $(SHELL) libtool
LIBTOOLFLAGS = --quiet
AWK = gawk
NORMAL_INSTALL = :
NORMAL_UNINSTALL = :

EXEEXT = 
DESTDIR = ${destdird}/${modulename}/rootfs

GO = /usr/bin/go
ifeq "${k__env_prod}" "${env}"
GOBUILD = ${GO} build -ldflags "-s -w"
else
GOBUILD = ${GO} build
endif
GOTEST = ${GO} test
GOENV = ${GO} env

am__base_list = \
  sed '$$!N;$$!N;$$!N;$$!N;$$!N;$$!N;$$!N;s/\n/ /g' | \
  sed '$$!N;$$!N;$$!N;$$!N;s/\n/ /g'
am__uninstall_files_from_dir = { \
  test -z "$$files" \
	|| { test ! -d "$$dir" && test ! -f "$$dir" && test ! -r "$$dir"; } \
	|| { echo " ( cd '$$dir' && rm -f" $$files ")"; \
		 $(am__cd) "$$dir" && rm -f $$files; }; \
  }

all: bin

gobuild-env:
export GOPATH=$(EXPORT_GOPATH)
export GOROOT=$(EXPORT_GOROOT)
export PKG_CONFIG_PATH=$(EXPORT_PKG_CONFIG_PATH)
export C_INCLUDE_PATH=$(EXPORT_C_INCLUDE_PATH)

bin: gobuild-env
	@list="$(bin_PROGRAMS)"; test -n "$(bin_PROGRAMS)" || exit 0; \
	m="$(GOBUILD_MAIN)"; for p in $$list; do \
		main=`echo $$m | sed 's/ .*$$//g'`; m=`echo $$m | sed "s/^$$main//g"`; \
		echo " $(GOBUILD) -o $$p $(GOBUILD_FLAGS) $$main"; \
		$(GOBUILD) -o $$p $(GOBUILD_FLAGS) $$main || exit $$?; \
	done;

test: gobuild-env
	@echo " $(GOTEST) $(GOTEST_FLAGS)"; $(GOTEST) $(GOTEST_FLAGS) || exit $$?;

clean: clean-binPROGRAMS
	@if test -e "$$($(GOENV) GOCACHE)"; then \
	  echo " rm -rf $$($(GOENV) GOCACHE)"; \
	  rm -rf $$($(GOENV) GOCACHE) || exit $$?; \
	fi;

uninstall: uninstall-binPROGRAMS uninstall-sysconfDATA uninstall-unitDATA

installdirs:
	for dir in "$(DESTDIR)$(bindir)"; do \
	  test -z "$$dir" || $(MKDIR_P) "$$dir"; \
	done

install: install-binPROGRAMS install-sysconfDATA install-unitDATA

install-binPROGRAMS: bin
	@$(NORMAL_INSTALL)
	@list='$(bin_PROGRAMS)'; test -n "$(bindir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(bindir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(bindir)" || exit 1; \
	fi; \
	for p in $$list; do echo "$$p $$p"; done | \
	sed 's/$(EXEEXT)$$//' | \
	while read p p1; do if test -f $$p \
	 || test -f $$p1 \
	  ; then echo "$$p"; echo "$$p"; else :; fi; \
	done | \
	sed -e 'p;s,.*/,,;n;h' \
		-e 's|.*|.|' \
		-e 'p;x;s,.*/,,;s/$(EXEEXT)$$//;$(transform);s/$$/$(EXEEXT)/' | \
	sed 'N;N;N;s,\n, ,g' | \
	$(AWK) 'BEGIN { files["."] = ""; dirs["."] = 1 } \
	  { d=$$3; if (dirs[d] != 1) { print "d", d; dirs[d] = 1 } \
		if ($$2 == $$4) files[d] = files[d] " " $$1; \
		else { print "f", $$3 "/" $$4, $$1; } } \
	  END { for (d in files) print "f", d, files[d] }' | \
	while read type dir files; do \
		if test "$$dir" = .; then dir=; else dir=/$$dir; fi; \
		test -z "$$files" || { \
		echo " $(LIBTOOL) $(AM_LIBTOOLFLAGS) $(LIBTOOLFLAGS) --mode=install $(INSTALL_PROGRAM) $$files '$(DESTDIR)$(bindir)$$dir'"; \
		$(LIBTOOL) $(AM_LIBTOOLFLAGS) $(LIBTOOLFLAGS) --mode=install $(INSTALL_PROGRAM) $$files "$(DESTDIR)$(bindir)$$dir" || exit $$?; \
		} \
	; done

uninstall-binPROGRAMS:
	@$(NORMAL_UNINSTALL)
	@list='$(bin_PROGRAMS)'; test -n "$(bindir)" || list=; \
	files=`for p in $$list; do echo "$$p"; done | \
	  sed -e 'h;s,^.*/,,;s/$(EXEEXT)$$//;$(transform)' \
		  -e 's/$$/$(EXEEXT)/' \
	`; \
	test -n "$$list" || exit 0; \
	echo " ( cd '$(DESTDIR)$(bindir)' && rm -f" $$files ")"; \
	cd "$(DESTDIR)$(bindir)" && rm -f $$files
	
clean-binPROGRAMS:
	@list='$(bin_PROGRAMS)'; test -n "$$list" || exit 0; \
	echo " rm -f" $$list; \
	rm -f $$list || exit $$?; \
	test -n "$(EXEEXT)" || exit 0; \
	list=`for p in $$list; do echo "$$p"; done | sed 's/$(EXEEXT)$$//'`; \
	echo " rm -f" $$list; \
	rm -f $$list

install-sysconfDATA: $(sysconf_DATA)
	@$(NORMAL_INSTALL)
	@list='$(sysconf_DATA)'; test -n "$(sysconfdir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(sysconfdir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(sysconfdir)" || exit 1; \
	fi; \
	for p in $$list; do \
	  dst=$(DESTDIR)$(sysconfdir); $(am__install_files_or_dirs); \
	done;

uninstall-sysconfDATA:
	@$(NORMAL_UNINSTALL)
	@list='$(sysconf_DATA)'; test -n "$(sysconfdir)" || list=; \
	for p in $$list; do \
	  dst=$(DESTDIR)$(sysconfdir); $(am__uninstall_files_or_dirs); \
	done;

install-unitDATA: $(unit_DATA)
	@$(NORMAL_INSTALL)
	@list='$(unit_DATA)'; test -n "$(unitdir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(unitdir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(unitdir)" || exit 1; \
	fi; \
	for p in $$list; do \
	  dst=$(DESTDIR)$(unitdir); $(am__install_files_or_dirs); \
	done;

uninstall-unitDATA:
	@$(NORMAL_UNINSTALL)
	@list='$(unit_DATA)'; test -n "$(unitdir)" || list=; \
	for p in $$list; do \
	  dst=$(DESTDIR)$(unitdir); $(am__uninstall_files_or_dirs); \
	done;
