package mq

import (
	"../conf"
	"errors"
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

// 向mosquitto中发送protobuf格式的消息
func Test_kafkamq(t *testing.T) {
	var err error
	var result string
	conf.KafkaConf = &conf.KafkaOptions{
		Server:      string("200.200.117.63:9092"),
		Topic:       []string{"moa"},
		GroupId:     "0",
		PollTimeout: 10,
		Routine: func(topic string, payload string) error {
			result = payload
			fmt.Println("kafka message: " + result)
			if result == "halo world" {
				return errors.New("normal exit")
			}
			return nil
		},
	}

	// mq
	mq := &KafkaReaderWriter{
		Conf: conf.KafkaConf,
	}

	Convey("kafka发布测试", t, func() {
		mq.InitProducer()

		err = mq.Publish([]byte("halo world"))
		So(err, ShouldBeNil)
	})

	Convey("kafka订阅测试", t, func() {
		mq.InitConsumer()

		err = mq.Subscribe()

		So(err, ShouldBeNil)
	})

}
