#!/bin/bash

function helpfunction() {
    echo "Usage: source devenv.sh [-e ENVIRONMENT]"
    echo -e "\t-e build dev, test, prod package, this will affect config file."
}

function buildncpus() {
    [ -z "$BUILD_NCPUS" ] \
        && BUILD_NCPUS="`/usr/bin/nproc 2>/dev/null || \
                         /usr/bin/getconf _NPROCESSORS_ONLN`";
    if [ "$BUILD_NCPUS" -gt 16 ]; then
        echo "-j16";
    elif [ "$BUILD_NCPUS" -gt 3 ]; then
        echo "-j$BUILD_NCPUS";
    else
        echo "-j3";
    fi
}

function installrpmmacros() {
    install -m 644 ${abs_top_builddir}/.rpmmacros "$HOME";
}

env=prod
while [ "$1" != "" ]; do
    case $1 in
        -e | --environment )    shift
                                environment=$1
                                ;;
        -h | --help )           helpfunction
                                ;;
        * )                     helpfunction
    esac
    shift
done

BUILD_PATH=$(dirname $BASH_SOURCE)
BUILD_PATH=$(realpath $BUILD_PATH)

abs_top_dir=${BUILD_PATH%/build*}

abs_top_3partydir=${abs_top_dir}/3party
abs_top_builddir=${abs_top_dir}/build
abs_top_deploydir=${abs_top_dir}/deploy
abs_top_mandir=${abs_top_dir}/man
abs_top_srcdir=${abs_top_dir}/src

abs_top_build_pkgdir=${abs_top_builddir}/pkg
abs_top_build_depdir=${abs_top_builddir}/dep
abs_top_build_makefiledir=${abs_top_builddir}/makefile.d

abs_top_deploy_pkgrpmdir=${abs_top_deploydir}/pkgrpm
abs_top_deploy_pkgdebdir=${abs_top_deploydir}/pkgdeb
abs_top_deploy_pkgtardir=${abs_top_deploydir}/pkgtar

abs_top_src_vardir=${abs_top_srcdir}/var
abs_top_src_libprotodir=${abs_top_srcdir}/libproto

export envd=${env}
export abs_top_dird=${BUILD_PATH%/build*}
export prefixd=/usr
export destdird=${abs_top_build_pkgdir}
echo "envd = ${env}
abs_top_dird = ${BUILD_PATH%/build*}
prefixd = /usr
destdird = ${abs_top_build_pkgdir}"

alias make="make -I ${abs_top_build_makefiledir} $(buildncpus)"
installrpmmacros

