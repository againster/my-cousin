import Vue from 'vue'
import Router from 'vue-router'
import MaterialStore from '@/components/material_store/'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/material',
      name: 'MaterialStore',
      component: MaterialStore
    }
  ]
})
