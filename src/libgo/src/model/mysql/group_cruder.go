package mysql

import "github.com/jinzhu/gorm"

type GroupCruder struct {}

func NewGroupCruder () *GroupCruder {
	return &GroupCruder{}
}

func (g *GroupCruder) QueryByGroupName(db *gorm.DB, name string) (*Group, error) {
	rt := &Group{}
	if db.Where("groupname = ?", name).Find(rt).RecordNotFound() {
		return nil, gorm.ErrRecordNotFound
	}
	return rt, db.Error
}