package apix

import (
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"golang.org/x/text/language"
	"github.com/BurntSushi/toml"
	"gopkg.in/yaml.v2"
	"encoding/json"
)

type I18nBundle struct {
	bundle *i18n.Bundle
	localeMap map[string]*i18n.Localizer
}

func (i *I18nBundle) NewI18nBundle(i18nFileFormat string,
	i18nFile ...string) error {
	i.bundle = i18n.NewBundle(language.English)
	return i.AppendI18nBundle(i18nFileFormat, i18nFile...)
}

func (i *I18nBundle) AppendI18nBundle(i18nFileFormat string,
	i18nFile ...string) error {
	var unmarshalFunc i18n.UnmarshalFunc
	switch i18nFileFormat {
	case KI18nFileFormatToml:
		unmarshalFunc = toml.Unmarshal
	case KI18nFileFormatYaml:
		unmarshalFunc = yaml.Unmarshal
	case KI18nFileFormatJson:
		unmarshalFunc = json.Unmarshal
	}
	i.bundle.RegisterUnmarshalFunc(i18nFileFormat, unmarshalFunc)

	for _, f := range i18nFile {
		_, err := i.bundle.LoadMessageFile(f)
		if err != nil {
			return err
		}
	}

	return nil
}

func (i *I18nBundle) Locale(lang string, msg *Message) *Message {
	return i.LocaleByTpl(lang, msg, nil)
}

func (i *I18nBundle) LocaleByTpl(lang string, msg *Message,
	tpl map[string]interface{}) *Message {
	return i.LocaleByRaw(lang, msg, &i18n.LocalizeConfig{MessageID: msg.ID,
		TemplateData: tpl})
}

func (i *I18nBundle) LocaleByPlural(lang string, msg *Message,
	tpl map[string]interface{}, plural interface{}) *Message {
	return i.LocaleByRaw(lang, msg, &i18n.LocalizeConfig{MessageID: msg.ID,
		TemplateData: tpl, PluralCount: plural})
}

func (i *I18nBundle) LocaleByRaw(lang string, msg *Message,
	conf *i18n.LocalizeConfig) *Message {
	locale, ok := i.localeMap[lang]
	if !ok {
		locale = i18n.NewLocalizer(i.bundle, lang)
		i.localeMap[lang] = locale
	}
	clone := msg.Clone()
	clone.Desc = locale.MustLocalize(conf)
	return clone
}