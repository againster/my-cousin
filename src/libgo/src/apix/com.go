package apix

const (
	KAcceptLanguage = "Accept-Language"
	KLocation       = "Location"

	KI18nFileFormatToml = "toml"
	KI18nFileFormatYaml = "yaml"
	KI18nFileFormatJson = "json"

	KDefaultLang = "zh-CN"
)
