package main

import (
	"flag"
	"router"
	"github.com/gin-gonic/gin"
	"conf"
	"go.uber.org/zap"
)

func main () {
	flag.Parse()
	conf.Logger, _ = zap.NewProduction()
	conf.Init()

	r := gin.Default()
	router.Init(r)

	r.Run(":8081")
}
