package apix

const (
	CodeOK = 0
)

type Message struct {
	Code int
	ID   string
	Desc string
}

var (
	MsgOK                           = &Message{CodeOK, "com.OK", ""}
	MsgAccessDenied                 = &Message{1, "com.AccessDenied", ""}
	MsgParamInvalid                 = &Message{2, "com.ParamInvalid", ""}
	MsgDbException                  = &Message{3, "com.DbException", ""}
	MsgNoSuchAccountOrWrongPassword = &Message{4, "com.NoSuchAccountOrWrongPassword", ""}
	MsgNoSuchGroupName              = &Message{5, "com.NoSuchGroupName", ""}
	MsgNoSuchAccountUnderGroup      = &Message{6, "com.NoSuchAccountUnderGroup", ""}
	MsgCanNotDeleteAccount          = &Message{7, "com.CanNotDeleteAccount", ""}
	MsgNoSuchAccount                = &Message{8, "com.NoSuchAccount", ""}
)

func (m *Message) Clone() *Message {
	return &Message{m.Code, m.ID, m.Desc,}
}

func (m *Message) New() *Message {
	return &Message{m.Code, m.ID, "",}
}
