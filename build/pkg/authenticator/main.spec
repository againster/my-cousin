Name:           es-authenticator
Version:        1.0.0
Release:        1%{?dist}
Summary:        eshine authenticator

License:        GPLv3
URL:            www.eshine.com
#Source0:       www.eshine.com/source/test0.tar.gz

#BuildRequires: gcc
BuildArch:      x86_64

%description
eshine authenticator. we change the world.
%changelog

%prep
%build
%install
%es_install %{moduledir}/rootfs
%es_filelist %{moduledir}/rootfs
%files -f %filelist
%clean
%es_clean
%pre
%post
%preun
%postun
