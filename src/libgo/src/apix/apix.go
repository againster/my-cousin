package apix

import (
	"go.uber.org/zap"
)

type ApiX struct {
	I18n *I18nBundle
	Log  *zap.Logger
	Gx   *GinX
}

func NewApiX(i *I18nBundle, log *zap.Logger, gx *GinX) *ApiX {
	return &ApiX{
		i, log, gx,
	}
}

func (x *ApiX) Lang() string {
	return KDefaultLang
}

/*
func (h *ApiX) ServeErr(ctx *gin.Context, err error, msg *Message) {
	//h.I18n.Locale(h.Gx.Lang(ctx), msg)
	h.Log.Warn(msg.Desc, zap.Error(err))
	h.Gx.ServeJSON(ctx, msg, nil)
}

func (h *ApiX) ServeJSON(ctx *gin.Context, data interface{},
	msg *Message) {
	//h.I18n.Locale(h.Gx.Lang(ctx), msg)
	h.Gx.ServeJSON(ctx, msg, data)
}
*/
