package sdk

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
	"mq"
)

func Test_email_sender_sdk(t *testing.T) {
	Convey("email sender sdk", t, func() {
		ka := &mq.KafkaReaderOptions{
			Server: "127.0.0.1:9092",
			Topic:"email-sender",
			GroupId:"1",
			Offset: "latest",
			PollTimeout: 500,
			SessionTimeout: 60000,
		}
		emailSdk, err := NewEmailSenderSdk(*ka)
		So(err, ShouldBeNil)

		_, err = emailSdk.Write("lz@eshine.com", "Little Bird", "uk", "eshine",
			"649250", 60)
		So(err, ShouldBeNil)

		err = emailSdk.Close()
		So(err, ShouldBeNil)
	})
}