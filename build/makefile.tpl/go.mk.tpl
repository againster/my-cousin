# program name
bin_PROGRAMS = email-sender
# config file that will be installed
sysconf_DATA = conf/email.ini.j2 i18n
# main package file, if only build one program, the value could be empty
GOBUILD_MAIN = main.go
# such as "-tags static", detail flags please see "go build help"
GOBUILD_FLAGS = -tags static
# such as "-v -cover -coverprofile=cover.out ./...", detail flags please see "go build help"
GOTEST_FLAGS = -v -cover -coverprofile=cover.out ./...
# system environment, like "GOPATH", etc. Only used for build time.
EXPORT_GOPATH = $(abs_moduledir):$(abs_top_srcdir)/libproto/go:$(abs_top_srcdir)/libgo:$(abs_top_3partydir)/libgo
EXPORT_GOROOT =
EXPORT_PKG_CONFIG_PATH = $(abs_top_build_depdir)/lib/pkgconfig
EXPORT_C_INCLUDE_PATH =

# program name
bin_PROGRAMS += captcha-sender
# main package file
GOBUILD_MAIN += captchasender.go

PACKAGE = email-sender
PACKAGE_BUGREPORT = lissaagainster@yahoo.com
PACKAGE_NAME = email-sender
PACKAGE_STRING = email-sender 1.0
PACKAGE_TARNAME = email-sender
PACKAGE_URL =
PACKAGE_VERSION = 1.0.0

include go.mk