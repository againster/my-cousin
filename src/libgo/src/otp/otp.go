package otp

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/binary"
	"net/url"
	"fmt"
	"encoding/base32"
	"strconv"
)

/*
Package dgoogauth implements the one-time password algorithms supported by Google Authenticator
This package supports the HMAC-Based One-time Password (HOTP) algorithm
specified in RFC 4226 and the Time-based One-time Password (TOTP) algorithm
specified in RFC 6238.
Refer to https://github.com/dgryski/dgoogauth
*/

// Much of this code assumes int == int64, which probably is not the case.

// computeCode computes the response code for a 64-bit challenge 'value' using the secret 'secret'.
// To avoid breaking compatibility with the previous API, it returns an invalid code (-1) when an error occurs,
// but does not silently ignore them (it forces a mismatch so the code will be rejected).
func computeCode(secret string, value int64) (int, error) {
	key, err := base32.StdEncoding.DecodeString(secret)
	if err != nil {
		return 0, err
	}

	hash := hmac.New(sha1.New, key)
	err = binary.Write(hash, binary.BigEndian, value)
	if err != nil {
		return 0, err
	}
	h := hash.Sum(nil)

	offset := h[19] & 0x0f
	truncated := binary.BigEndian.Uint32(h[offset: offset+4])

	truncated &= 0x7fffffff
	code := truncated % 1000000

	fmt.Println(code)
	return int(code), nil
}

type Hotp struct {
	windowSize int
	secret     string
}

func NewHotp(secret string, windowSize int) *Hotp {
	return &Hotp{
		secret:     secret,
		windowSize: windowSize,
	}
}

func (c *Hotp) CheckCode(counter int64, otpCode int) (int64, bool) {

	for i := 0; i < c.windowSize; i++ {
		code, err := computeCode(c.secret, counter+int64(i))
		if err != nil {
			return 0, false
		}

		if otpCode == code {
			// We don't check for overflow here, which means you can only authenticate 2^63 times
			// After that, the counter is negative and the above 'if' test will fail.
			// This matches the behaviour of the PAM module.
			return counter + int64(i+1), true
		}
	}

	// we must always advance the counter if we tried to authenticate with it
	return counter + 1, false
}

// ProvisionURIWithIssuer generates a URI that can be turned into a QR code
// to configure a Google Authenticator mobile app. It respects the recommendations
// on how to avoid conflicting accounts.
//
// See https://github.com/google/google-authenticator/wiki/Conflicting-Accounts
func (c *Hotp) provisionURIWithIssuer(user string, counter int64,
	issuer string) string {
	auth := "hotp/"
	q := make(url.Values)
	q.Add("secret", c.secret)
	if counter > 0 {
		auth = "hotp/"
		q.Add("counter", strconv.FormatInt(counter, 10))
	}
	if issuer != "" {
		q.Add("issuer", issuer)
		auth += issuer + ":"
	}


	return "otpauth://" + auth + user + "?" + q.Encode()
}

func (c *Hotp) ProvisionURI(user string, counter int64) string {
	return c.provisionURIWithIssuer(user, counter,"")
}

type Totp struct {
	windowSize     int
	secret         string
	refreshSeconds int64
}

func NewTotp(secret string, windowSize int) *Totp {
	return &Totp{
		secret:         secret,
		windowSize:     windowSize,
		refreshSeconds: 30,
	}
}

func (c *Totp) CheckCode(timestamp int64, otpCode int) bool {
	minT := timestamp/c.refreshSeconds - int64(c.windowSize)
	maxT := timestamp/c.refreshSeconds + int64(c.windowSize)

	for t := minT; t <= maxT; t++ {
		code, err := computeCode(c.secret, int64(t))
		if err != nil {
			fmt.Println(err)
			return false
		}

		if code == otpCode {
			return true
		}
	}

	return false
}

// ProvisionURIWithIssuer generates a URI that can be turned into a QR code
// to configure a Google Authenticator mobile app. It respects the recommendations
// on how to avoid conflicting accounts.
//
// See https://github.com/google/google-authenticator/wiki/Conflicting-Accounts
func (c *Totp) provisionURIWithIssuer(user string, issuer string) string {
	auth := "totp/"
	q := make(url.Values)
	q.Add("secret", c.secret)
	if issuer != "" {
		q.Add("issuer", issuer)
		auth += issuer + ":"
	}

	return "otpauth://" + auth + user + "?" + q.Encode()
}

func (c *Totp) ProvisionURI(user string) string {
	return c.provisionURIWithIssuer(user, "")
}
