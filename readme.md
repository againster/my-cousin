
# eshine 工程说明

## 编译环境依赖

```bash
# 编译工具
yum install -y gcc #编译c语言
yum install -y golang #编译go语言
yum install -y protobuf #生成proto

# rpm包构建工具
yum install -y rpmdevtools #rpm包目录树等初始化工具
yum install -y rpm-build #构建rpm包
yum install -y rpmlint #检查rpm包合法与完整性

# pip 工具
yum install -y python-pip #python安装工具
## 配置pip源，请替换为更快的镜像源
cat << __END__ > /etc/pip.conf
[global]
index-url = http://mirrors.sangfor.org/pypi/simple
extra-index-url = http://mirrors.sangfor.org/sfpypi/simple
[install]
trusted-host = mirrors.sangfor.org
__END__
pip install --upgrade pip #升级pip
pip install jinja2-cli #jinjia2模板，实例化配置文件

# api test工具
newman
```

## CI/CD流程

build->pack->deploy->test