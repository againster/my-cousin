package apix

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type GinX struct {}

func NewGinEx() *GinX {
	return &GinX{}
}

type Ret struct {
	Code int         `json:"code"`
	Desc string      `json:"desc"`
	Data interface{} `json:"data"`
}

func (g *GinX) ServeJSON(ctx *gin.Context, msg *Message, data interface{}) {
	ret := &Ret{
		Code: msg.Code,
		Desc: msg.Desc,
		Data: data,
	}
	status := ctx.Writer.Status()
	if status == 0 {
		status = http.StatusOK
	}
	ctx.JSON(status, ret)
}

func (g *GinX) Lang(ctx *gin.Context) string {
	lang := ctx.GetHeader(KAcceptLanguage)
	if lang != "" {
		return lang
	}
	return KDefaultLang
}

