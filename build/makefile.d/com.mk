# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUGHOR: ArrowLee
# DATE: 20190923

k__env_prod = prod

mkdir_p = $(MKDIR_P)
MKDIR_P = /usr/bin/mkdir -p

install_sh_DATA = $(install_sh) -c -m 644
install_sh_PROGRAM = $(install_sh) -c
install_sh_SCRIPT = $(install_sh) -c
install_sh = $(INSTALL)
INSTALL = /usr/bin/install -c
INSTALL_DATA = ${INSTALL} -m 644
INSTALL_PROGRAM = ${INSTALL}

# $$f: be striped path. /root/.test.j2 -> .test.j2
am__strip_dir = `echo $$f | sed -e 's|^.*/||'`
# $$f: be striped path. /root/.test.j2 -> /root
am__strip_basename = `echo $$f | sed -e 's|/[^/]*$$||;'`
# $$f: be striped path. /root/.test.j2 -> .j2
am__strip_base = `echo $$f | sed -e 's|^.*\.|\.|'`
# $$f: be striped path. /root/.test.j2 -> /root/.test
am__strip_suffix = `echo $$f | sed -e 's|\.[^.]*$$||'`
# $$f: be striped path. ~/.test.j2 -> .test.j2; ./test.j2 -> test.j2; . -> ;
am__strip_relative_prefix = `echo $$f | sed -e "s,^\(\./\|\.\./\|~\|~/\)*,,;s,^\.$$,,;"`

am__cd = CDPATH="$${ZSH_VERSION+.}$(PATH_SEPARATOR)" && cd
am__mv = mv -f

#$$f: file content
am__md5sum = `echo $$f | $(MD5SUM) | awk '{print $1}'`
#$$f: file name
am__md5sumfile = `$(MD5SUM) $$f | awk '{print $1}'`

MD5SUM = /usr/bin/md5sum -b
CAT = /usr/bin/cat
J2CLI = /usr/bin/jinja2
J2Render = true
J2EXT = .j2
J2RenderMax = 10


## $$p: be installed dir or file, $$dst: install target
am__install_files_or_dirs = \
  if test -d "$$p"; then \
	dirs=$$(find $$p -type d -printf "%p "); \
	for dir in $$dirs; do \
      f=$$dir; dir=$(am__strip_relative_prefix); test -n "$$dir" || continue; \
	  if ! test -d "$$dst/$$dir"; then \
	    echo " $(MKDIR_P) '$$dst/$$dir'"; $(MKDIR_P) "$$dst/$$dir" || exit 1; \
	  fi; \
	done; \
	files=$$(find $$p -type f -printf "%p "); \
	tmpdst=$$dst; for file in $$files; do \
      f=$$file; f=$(am__strip_relative_prefix); test -n "$$f" || continue; \
	  dst=$$tmpdst/$(am__strip_basename); $(am__install_j2_template_file); \
	done; dst=$$tmpdst;\
  else \
	file=$$p; $(am__install_j2_template_file); \
  fi

## $$p: be installed dir or file, $$dst: install target
am__uninstall_files_or_dirs = \
  if test -d "$$p"; then \
    files=$$(find $$p -type f -printf "%p "); \
  	for file in $$files; do \
  	  f=$$file; f=$(am__strip_relative_prefix); test -n "$$f" || continue; \
  	  $(am__uninstall_j2_template_file); \
  	done; \
  else \
  	file=$$p; $(am__uninstall_j2_template_file); \
  fi \

## $$f: be asserted file
am__test_j2_template_file = \
  test $(J2Render) == "true" && test $(am__strip_base) == $(J2EXT)

## $$file: be rendered file, $$dst: install target dir
am__install_j2_template_file = \
  f=$$file; if $(am__test_j2_template_file); then \
	f=$$file; f=$(am__strip_dir); out=$$dst/$(am__strip_suffix); \
	$(am__render_j2_template_file); \
  else \
	echo " $(INSTALL_DATA) $$file '$$dst'"; \
	$(INSTALL_DATA) $$file "$$dst" || exit $$?; \
  fi

## $$file: be rendered file, $$dst: install target dir
am__uninstall_j2_template_file = \
  f=$$file; if $(am__test_j2_template_file); then \
	f=$$file; f=$(am__strip_dir); out=$(am__strip_suffix); \
	echo " ( cd '$$dst' && rm -f $$out )"; \
	$(am__cd) "$$dst" && rm -f $$out || exit $$?; \
  else \
	echo " ( cd '$$dst' && rm -f $$file )"; \
	$(am__cd) "$$dst" && rm -f $$file || exit $$?; \
  fi

## $$file: be rendered file, $$out: output filename
am__render_j2_template_file = \
  tmpj2=$$file; f=$$tmpj2; oldsum=$(am__md5sumfile); \
  for ((i=1;i<=$(J2RenderMax);i++)); do \
    tmpj2out=$$out.$$i; j2last=`expr $$i - 1`; tmpj2last=$$out.$$j2last; \
	echo " $(CAT) $(envvardir)/*.yml | $(J2CLI) -o '$$tmpj2out' '$$tmpj2'"; \
	$(CAT) $(envvardir)/*.yml | $(J2CLI) -o $$tmpj2out $$tmpj2 || exit $$?; \
    f=$$tmpj2out; newsum=$(am_md5sumfile); \
    test $$j2last -gt 0 && { echo " rm -f $$tmpj2last"; rm -f $$tmpj2last; }; \
    test "$$oldsum" = "$$newsum" && { echo " $(am__mv) $$tmpj2out $$out"; \
      $(am__mv) $$tmpj2out $$out; break; }; \
    tmpj2=$$tmpj2out; oldsum=$$newsum; \
  done

