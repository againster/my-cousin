package mysql

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"fmt"
	"time"
)

type MysqlOptions struct {
	Url string `ini:"url"`
}

var dbPool = make(map[string]*gorm.DB)
var nowDB *gorm.DB

func NewMysql(op *MysqlOptions, alias string) (*gorm.DB, error) {
	_, ok := dbPool[alias]
	if ok {
		return nil, fmt.Errorf("db %s already exists", alias)
	}
	db, err := gorm.Open("mysql", op.Url)
	if err != nil {
		return nil, err
	}
	nowDB = db
	dbPool[alias] = db
	db.DB().SetConnMaxLifetime(time.Hour * 3)
	db.DB().SetMaxIdleConns(2)
	db.DB().SetMaxOpenConns(100)
	return db, nil
}

func UseDB(alias string) *gorm.DB {
	return dbPool[alias]
}

func DefaultDB() *gorm.DB {
	return dbPool["default"]
}

func NewDB() *gorm.DB {
	return nowDB.New()
}
