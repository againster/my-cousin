package conf

import (
	"go.uber.org/zap"
	"os"
	"flag"
	"gopkg.in/ini.v1"
	"logger"
	"model/mysql"
)

var Logger *zap.Logger
var LogConf = &logger.LogOptions{}
var CmdConf = &CmdOptions{}
var MysqlConf = &mysql.MysqlOptions{}

// cmd options
type CmdOptions struct {
	Conffile string
	I18nDir  string
}

// init global variables
func init() {
	flag.StringVar(&CmdConf.Conffile, "c", "conf/material.ini",
		"app config file path")
	flag.StringVar(&CmdConf.I18nDir, "i", "i18n",
		"app i18n dir path")
}

func Init() {
	var err error
	iniFile, err := ini.Load(CmdConf.Conffile)
	if err != nil {
		Logger.Error("conf file error", zap.Error(err))
		os.Exit(1)
	}
	initLog(iniFile)
	initMysql(iniFile)
}

func initLog(iniFile *ini.File) {
	err := iniFile.Section("log").MapTo(LogConf)
	if err != nil {
		Logger.Error("conf log error", zap.Error(err))
		os.Exit(1)
	}
	Logger, err = logger.NewZapLogger(LogConf)
	if err != nil {
		Logger.Error("conf zap error", zap.Error(err))
		os.Exit(1)
	}
}

func initMysql(iniFile *ini.File) {
	err := iniFile.Section("mysql").MapTo(MysqlConf)
	if err != nil {
		Logger.Error("conf mysql error", zap.Error(err))
		os.Exit(1)
	}
	Logger.Debug("mysql conf", zap.Any("mysql", MysqlConf))
	_, err = mysql.NewMysql(MysqlConf, "default")
	if err != nil {
		Logger.Error("conf mysql error", zap.Error(err))
		os.Exit(1)
	}
}
