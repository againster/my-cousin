package model

type Material struct {
	Id           int64   `gorm:"column:id;AUTO_INCREMENT;primary_key;"`
	Ctime        int64   `gorm:"column:ctime"`
	Name         string  `gorm:"column:name"`
	UnitPrice    float64 `gorm:"column:unit_price"`
	ProjName     string  `gorm:"column:proj_name"`
	Model        string  `gorm:"column:model"`
	TotalPrice   float64 `gorm:"column:total_price"`
	MaxPrice     float64 `gorm:"column:max_price"`
	MinPrice     float64 `gorm:"column:min_price"`
	StoreType    string  `gorm:"column:store_type"`
	MaterialType string  `gorm:"column:material_type"`
	Buyer        string  `gorm:"column:buyer"`
	Keeper       string  `gorm:"column:keeper"`
	StoreNo      string  `gorm:"column:store_no"`
	MeasureUnit  string  `gorm:"column:measure_unit"`
	Inspector    string  `gorm:"column:inspector"`
	Supplier     string  `gorm:"column:supplier"`
	MaterialNo   string  `gorm:"column:material_no"`
	Total        float64 `gorm:"column:total"`
	Remaining    float64 `gorm:"column:remaining"`
	Comment      string  `gorm:"column:comment"`
}

func (m *Material) TableName () string {
	return "material";
}
