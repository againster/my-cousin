package mysql

import (
	"util"
	"encoding/base64"
	"strings"
	"fmt"
	"crypto/aes"
)

const kDelimiter = "$"

type AccountHelper struct{}

func NewAccountHelper() (*AccountHelper) {
	return &AccountHelper{}
}

func (c *AccountHelper) CheckPassword(plain, hash string) (bool, error) {
	u := util.NewUnixCrypt()
	r, s, _, err := u.ParseString(hash)
	if err != nil {
		return false, err
	}

	p, err := u.HashWithRound([]byte(plain), []byte(s), r)
	if err != nil {
		return false, err
	}

	return p == hash, nil
}

func (c *AccountHelper) HashPassword(plain string) (string, error) {
	u := util.NewUnixCrypt()
	salt, err := u.MkSalt()
	if err != nil {
		return "", err
	}

	p, err := u.Hash([]byte(plain), []byte(salt))
	if err != nil {
		return "", err
	}
	return p, nil
}

func (c *AccountHelper) EncodeQuestionAnswer(q, a string) (string, error) {
	ahash, err := c.HashPassword(a)
	if err != nil {
		return "", err
	}
	qbase := base64.StdEncoding.EncodeToString([]byte(q))
	return kDelimiter + qbase + ahash, nil
}

func (c *AccountHelper) CheckQuestionAnswer(q, a, encoded string) (bool,
	error) {

	question, ahash, err := c.parseQuestionAnswer(encoded)
	if err != nil {
		return false, err
	}

	if question != base64.StdEncoding.EncodeToString([]byte(q)) {
		return false, fmt.Errorf("inconsistent question")
	}

	return c.CheckPassword(a, ahash)
}

func (c *AccountHelper) EncryptPhone(plain string) (string,
	error) {
	b, err := aes.NewCipher([]byte("6789A")) //todo
	if err != nil {
		return "", err
	}
	var rt []byte
	b.Encrypt(rt, []byte(plain))
	return string(rt), nil
}

func (c *AccountHelper) EncryptEmail(plain string) (string,
	error) {
	b, err := aes.NewCipher([]byte("12345")) //todo
	if err != nil {
		return "", err
	}
	var rt []byte
	b.Encrypt(rt, []byte(plain))
	return string(rt), nil
}

func (c *AccountHelper) parseQuestionAnswer(hash string) (question string,
	answer string, err error) {
	s := strings.SplitN(hash, kDelimiter, 2)
	if len(s) != 3 {
		return "", "", fmt.Errorf("invalid hash of question answer")
	}
	return s[1], s[2], nil
}
