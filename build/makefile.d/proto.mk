# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUGHOR: ArrowLee
# DATE: 20190923

include env.mk
include com.mk

protoccdir = ${abs_top_src_libprotodir}/cc
protogodir = ${abs_top_src_libprotodir}/go/src
protopydir = ${abs_top_src_libprotodir}/py

PROTOC = /usr/bin/protoc
PROTOS = $(wildcard $(abs_top_src_libprotodir:%=*.proto))

all: proto-cc proto-py proto-go

proto-cc:
	@if ! test -d "$(protoccdir)"; then \
	  echo " $(MKDIR_P) '$(protoccdir)'"; \
	  $(MKDIR_P) "$(protoccdir)" || exit $$?; \
	fi; \
	echo " $(PROTOC) --cpp_out=$(protoccdir) $(PROTOS)"; \
	$(PROTOC) --cpp_out=$(protoccdir) $(PROTOS) || $$?;

proto-py:
	@if ! test -d "$(protopydir)"; then \
	  echo " $(MKDIR_P) '$(protopydir)'"; \
	  $(MKDIR_P) "$(protopydir)" || exit $$?; \
	fi; \
	echo " $(PROTOC) --python_out=$(protopydir) $(PROTOS)"; \
	$(PROTOC) --python_out=$(protopydir) $(PROTOS) || $$?;

proto-go:
	@for proto in $(PROTOS); do \
	  protopkg=$$(sed -n "/^package .*;/p" $$proto | \
	  sed -E "s/^package (.*);/\1/g" | sed "s/\./_/g" | sed 's/\r//g'); \
	  if ! test -d "$(protogodir)/$$protopkg"; then \
	    echo " $(MKDIR_P) '$(protogodir)/$$protopkg'"; \
	    $(MKDIR_P) "$(protogodir)/$$protopkg" || exit $$?; \
	  fi; \
	  echo " $(PROTOC) --go_out=$(protogodir)/$$protopkg $$proto"; \
	  $(PROTOC) --go_out=$(protogodir)/$$protopkg $$proto || $$?; \
	done;

install:

uninstall:

clean:
