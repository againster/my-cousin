package util

import (
	"strings"
	"strconv"
	"crypto/sha512"
	"io"
	"fmt"
	"crypto/rand"
)

/*
UnixCrypt refer to
- https://akkadia.org/drepper/SHA-crypt.txt
- https://en.wikipedia.org/wiki/Crypt_(C)
 */
type UnixCrypt struct {}

type unixCryptOption struct {
	kDefaultRound  int
	kMinRound      int
	kMaxRound      int
	kSaltLenMax    int
	kUnixCryptCode string
	kMapTable      []byte
	inited         bool
}

var unixCryptConf = &unixCryptOption{
	kDefaultRound:  5000,
	kMinRound:      1000,
	kMaxRound:      999999999,
	kSaltLenMax:    16,
	kUnixCryptCode: "6",
	kMapTable: []byte{
		'.', '/', '0', '1', '2', '3', '4', '5', '6', '7',
		'8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
		'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
		'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b',
		'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
		'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
		'w', 'x', 'y', 'z'},
}

func NewUnixCrypt() *UnixCrypt {
	return &UnixCrypt{}
}

func (c *UnixCrypt) ParseString(raw string) (round int, salt string,
	hash string, err error) {
	s := strings.Split(raw, "$")
	if len(s) == 5 && s[1] == unixCryptConf.kUnixCryptCode {
		r := strings.Replace(s[2], "rounds=", "", -1)
		n, err := strconv.Atoi(r)
		if err != nil {
			return 0, "", "", fmt.Errorf("invalid round string")
		}
		return n, s[3], s[4], nil
	}

	if len(s) == 4 && s[1] == unixCryptConf.kUnixCryptCode {
		return unixCryptConf.kDefaultRound, s[2], s[3], nil
	}
	return 0, "", "", fmt.Errorf("invalid unix crypt format")
}

func (c *UnixCrypt) Hash(key []byte, salt []byte) (string, error) {
	return c.HashWithRound(key, salt, unixCryptConf.kDefaultRound)
}

func (c *UnixCrypt) HashWithRound(key []byte, salt []byte, round int) (string,
	error) {
	// limit round
	if round >= unixCryptConf.kMaxRound {
		round = unixCryptConf.kMaxRound
	} else if round <= unixCryptConf.kMinRound {
		round = unixCryptConf.kMinRound
	}
	// limit salt len
	if len(salt) > unixCryptConf.kSaltLenMax {
		salt = salt[:unixCryptConf.kSaltLenMax]
	}
	s, err := c.hash(key, salt, round)
	if err != nil {
		return "", err
	}
	b := strings.Builder{}
	b.WriteString("$")
	b.WriteString(unixCryptConf.kUnixCryptCode)
	if round != unixCryptConf.kDefaultRound {
		b.WriteString("$rounds=")
		b.WriteString(strconv.Itoa(round))
	}
	b.WriteString("$")
	b.Write(salt)
	b.WriteString("$")
	b.WriteString(s)
	return b.String(), nil
}

/*
Hash implements linux c function crypt.
password restriction recommendation:
1. minimum length: 8
2. maximum length: 160.
refer to https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html
 */
func (c *UnixCrypt) hash(key []byte, salt []byte, round int) (string, error) {
	lenKey := len(key)
	lenSalt := len(salt)
	rt := strings.Builder{}

	/* Prepare for the real work.  */
	ctx := sha512.New()
	ctx.Write(key)
	ctx.Write(salt)

	/* Compute alternate SHA512 sum with input KEY, SALT, and KEY.  The
	final result will be added to the first context.  */
	altCtx := sha512.New()
	altCtx.Write(key)
	altCtx.Write(salt)
	altCtx.Write(key)
	altResult := altCtx.Sum(nil)

	/* Add for any character in the key one byte of the alternate sum.  */
	cnt := lenKey
	for ; cnt > 64; cnt -= 64 {
		ctx.Write(altResult)
	}
	ctx.Write(altResult[:cnt])

	/* Take the binary representation of the length of the key and for every
     1 add the alternate sum, for every 0 the key.  */
	for cnt := lenKey; cnt > 0; cnt >>= 1 {
		if cnt&1 != 0 {
			ctx.Write(altResult)
		} else {
			ctx.Write(key)
		}
	}

	/* Create intermediate result.  */
	altResult = ctx.Sum(nil)

	/* Start computation of P byte sequence.  */
	altCtx = sha512.New()
	for cnt = 0; cnt < lenKey; cnt++ {
		altCtx.Write(key)
	}
	/* Finish the digest.  */
	tmpResult := altCtx.Sum(nil)

	/* Create byte sequence P.  */
	pcp := make([]byte, 0, lenKey)
	for cnt = lenKey; cnt >= 64; cnt -= 64 {
		pcp = append(pcp, tmpResult[:64]...)
	}
	pcp = append(pcp, tmpResult[:cnt]...)

	/* Start computation of S byte sequence.  */
	altCtx = sha512.New()
	/* For every character in the password add the entire password.  */
	for cnt := 0; cnt < 16+int(altResult[0]); cnt++ {
		altCtx.Write([]byte(salt))
	}
	/* Finish the digest.  */
	tmpResult = altCtx.Sum(nil)

	/* Create byte sequence S.  */
	scp := make([]byte, 0, lenSalt)
	for cnt = lenSalt; cnt >= 64; cnt -= 64 {
		scp = append(scp, tmpResult...)
	}
	scp = append(scp, tmpResult[:cnt]...)

	for cnt = 0; cnt < round; cnt ++ {
		ctx = sha512.New()
		/* Add key or last result.  */
		if cnt&1 != 0 {
			ctx.Write(pcp[:lenKey])
		} else {
			ctx.Write(altResult[:64])
		}

		if cnt%3 != 0 {
			ctx.Write(scp[:lenSalt])
		}

		/* Add key for numbers not divisible by 7.  */
		if cnt%7 != 0 {
			ctx.Write(pcp[:lenKey])
		}

		/* Add key or last result.  */
		if cnt&1 != 0 {
			ctx.Write(altResult[:64])
		} else {
			ctx.Write(pcp[:lenKey])
		}

		/* Create intermediate result.  */
		altResult = ctx.Sum(nil)
	}
	rt.Write(c.b64From24Bit(altResult[0], altResult[21], altResult[42], 4))
	rt.Write(c.b64From24Bit(altResult[22], altResult[43], altResult[1], 4))
	rt.Write(c.b64From24Bit(altResult[44], altResult[2], altResult[23], 4))
	rt.Write(c.b64From24Bit(altResult[3], altResult[24], altResult[45], 4))
	rt.Write(c.b64From24Bit(altResult[25], altResult[46], altResult[4], 4))
	rt.Write(c.b64From24Bit(altResult[47], altResult[5], altResult[26], 4))
	rt.Write(c.b64From24Bit(altResult[6], altResult[27], altResult[48], 4))
	rt.Write(c.b64From24Bit(altResult[28], altResult[49], altResult[7], 4))
	rt.Write(c.b64From24Bit(altResult[50], altResult[8], altResult[29], 4))
	rt.Write(c.b64From24Bit(altResult[9], altResult[30], altResult[51], 4))
	rt.Write(c.b64From24Bit(altResult[31], altResult[52], altResult[10], 4))
	rt.Write(c.b64From24Bit(altResult[53], altResult[11], altResult[32], 4))
	rt.Write(c.b64From24Bit(altResult[12], altResult[33], altResult[54], 4))
	rt.Write(c.b64From24Bit(altResult[34], altResult[55], altResult[13], 4))
	rt.Write(c.b64From24Bit(altResult[56], altResult[14], altResult[35], 4))
	rt.Write(c.b64From24Bit(altResult[15], altResult[36], altResult[57], 4))
	rt.Write(c.b64From24Bit(altResult[37], altResult[58], altResult[16], 4))
	rt.Write(c.b64From24Bit(altResult[59], altResult[17], altResult[38], 4))
	rt.Write(c.b64From24Bit(altResult[18], altResult[39], altResult[60], 4))
	rt.Write(c.b64From24Bit(altResult[40], altResult[61], altResult[19], 4))
	rt.Write(c.b64From24Bit(altResult[62], altResult[20], altResult[41], 4))
	rt.Write(c.b64From24Bit(0, 0, altResult[63], 2))
	/* clear */
	ctx = sha512.New()
	ctx.Sum(nil)
	return rt.String(), nil
}

func (c *UnixCrypt) b64From24Bit(b2, b1, b0 byte, n int) []byte {
	rt := make([]byte, 0, n)
	w := uint32(b2)<<16 | uint32(b1)<<8 | uint32(b0)
	for ; n > 0; n-- {
		rt = append(rt, unixCryptConf.kMapTable[w&0x3F]);
		w >>= 6;
	}
	return rt
}

func (c *UnixCrypt) MkSalt() (string, error) {
	return c.mkSalt(unixCryptConf.kSaltLenMax)
}

func (c *UnixCrypt) MkRound() (int, error) {
	return c.mkRound(unixCryptConf.kMinRound, unixCryptConf.kMaxRound)
}

func (c *UnixCrypt) mkSalt(length int) (string, error) {
	b := make([]byte, length)
	_, err := io.ReadFull(rand.Reader, b)
	if err != nil {
		return "", err
	}

	rt := make([]byte, length)
	for k, v := range b {
		rt[k] = unixCryptConf.kMapTable[v>>2]
	}

	return string(rt), nil
}

func (c *UnixCrypt) mkRound(min int, max int) (int, error) {
	b := make([]byte, 4)
	_, err := io.ReadFull(rand.Reader, b)
	if err != nil {
		return 0, err
	}

	rt := int(b[0])<<24 | int(b[1])<<16 | int(b[2])<<8 | int(b[3])

	return rt, nil
}
