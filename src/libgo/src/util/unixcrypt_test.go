package util

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_mksalt(t *testing.T) {
	Convey("mksalt", t, func() {
		l := NewUnixCrypt()
		n, err := l.mkSalt(10)
		So(err, ShouldBeNil)
		So(len(n), ShouldEqual, 10)
	})
}

func Test_parsestring(t *testing.T) {
	Convey("parsestring", t, func() {
		l := NewUnixCrypt()
		testCase := []struct{
			in string
			r int
			salt string
			hash string
		} {
			{
				in: "$6$rounds=77777$short$WuQyW2YR.hBNpjjRhpYD/ifIw05xdfeEyQoMxIXbkvr0gge1a1x3yRULJ5CCaUeOxFmtlcGZelFl5CxtgfiAc0",
				r: 77777,
				salt: "short",
				hash: "WuQyW2YR.hBNpjjRhpYD/ifIw05xdfeEyQoMxIXbkvr0gge1a1x3yRULJ5CCaUeOxFmtlcGZelFl5CxtgfiAc0",
			},
			{
				in: "$6$saltstring$svn8UoSVapNtMuq1ukKS4tPQd8iKwSMHWjl/O817G3uBnIFNjnQJuesI68u4OTLiBFdcbYEdFCoEOfaS35inz1",
				r: unixCryptConf.kDefaultRound,
				salt: "saltstring",
				hash: "svn8UoSVapNtMuq1ukKS4tPQd8iKwSMHWjl/O817G3uBnIFNjnQJuesI68u4OTLiBFdcbYEdFCoEOfaS35inz1",
			},
		}
		for _, v := range testCase{
			r, salt, hash, err := l.ParseString(v.in)
			So(err, ShouldBeNil)
			So(r, ShouldEqual, v.r)
			So(salt, ShouldEqual, v.salt)
			So(hash, ShouldEqual, v.hash)
		}
	})
}

func Test_hash(t *testing.T) {
	// the test case refer to https://akkadia.org/drepper/SHA-crypt.txt
	Convey("hash", t, func() {
		l := NewUnixCrypt()
		testCase := []struct {
			key  string
			salt string
			round int
			out  string
		}{
			{
				key:  "Hello world!",
				salt: "saltstring",
				round: unixCryptConf.kDefaultRound,
				out:  "$6$saltstring$svn8UoSVapNtMuq1ukKS4tPQd8iKwSMHWjl/O817G3uBnIFNjnQJuesI68u4OTLiBFdcbYEdFCoEOfaS35inz1",
			},
			{
				key:  "Hello world!",
				salt: "saltstringsaltstring",
				round: 10000,
				out:  "$6$rounds=10000$saltstringsaltst$OW1/O6BYHV6BcXZu8QVeXbDWra3Oeqh0sbHbbMCVNSnCM/UrjmM0Dp8vOuZeHBy/YTBmSK6H9qs/y3RnOaw5v.",
			},
			{
				key:  "the minimum number is still observed",
				salt: "roundstoolow",
				round: 10,
				out:  "$6$rounds=1000$roundstoolow$kUMsbe306n21p9R.FRkW3IGn.S9NPN0x50YhH1xhLsPuWGsUSklZt58jaTfF4ZEQpyUNGc0dqbpBYYBaHHrsX.",
			},
			{
				key:  "we have a short salt string but not a short password",
				salt: "short",
				round: 77777,
				out:  "$6$rounds=77777$short$WuQyW2YR.hBNpjjRhpYD/ifIw05xdfeEyQoMxIXbkvr0gge1a1x3yRULJ5CCaUeOxFmtlcGZelFl5CxtgfiAc0",
			},
			{
				key:  "a short string",
				salt: "asaltof16chars..",
				round: 123456,
				out:  "$6$rounds=123456$asaltof16chars..$BtCwjqMJGx5hrJhZywWvt0RLE8uZ4oPwcelCjmw2kSYu.Ec6ycULevoBK25fs2xXgMNrCzIMVcgEJAstJeonj1",
			},
			{
				key:  "a very much longer text to encrypt.  This one even stretches over morethan one line.",
				salt: "anotherlongsaltstring",
				round: 1400,
				out:  "$6$rounds=1400$anotherlongsalts$POfYwTEok97VWcjxIiSOjiykti.o/pQs.wPvMxQ6Fm7I6IoYN3CmLs66x9t0oSwbtEW7o7UmJEiDwGqd8p4ur1",
			},
			{
				key:  "This is just a test",
				salt: "toolongsaltstring",
				round: unixCryptConf.kDefaultRound,
				out:  "$6$toolongsaltstrin$lQ8jolhgVRVhY4b5pZKaysCLi0QBxGoNeKQzQ3glMhwllF7oGDZxUhx1yxdYcz/e1JSbq3y6JMxxl8audkUEm0",
			},
		}
		for _, v := range testCase {
			rt, err := l.HashWithRound([]byte(v.key), []byte(v.salt), v.round)
			So(err, ShouldBeNil)
			So(rt, ShouldEqual, v.out)
		}
	})
}