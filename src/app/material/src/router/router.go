package router

import (
	"github.com/gin-gonic/gin"
	"controller"
)

func Init(engine *gin.Engine) {
	// Simple group: v2
	m := controller.NewMaterial()
	v1 := engine.Group("/material")
	{
		v1.GET("/read", m.Read)
		v1.POST("/new", m.New)
		v1.DELETE("/delete", m.Delete)
		v1.PUT("/update", m.Update)
	}
}
