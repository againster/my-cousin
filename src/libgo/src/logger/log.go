package logger

import (
	"go.uber.org/zap"
)

type LogOptions struct {
	LogLevel string `ini:"loglevel"`
	LogFile  string `ini:"logfile"`
}

func NewZapLogger(op *LogOptions) (*zap.Logger, error) {
	level := zap.NewAtomicLevel()
	err := level.UnmarshalText([]byte(op.LogLevel))
	if err != nil {
		return nil, err
	}

	zapConf := &zap.Config{
		Level:             level,
		Development:       false,
		DisableCaller:     false,
		DisableStacktrace: true,
		Sampling:          nil,
		Encoding:          "json",
		EncoderConfig:     zap.NewProductionEncoderConfig(),
		OutputPaths:       []string{op.LogFile},
		ErrorOutputPaths:  []string{op.LogFile},
	}

	logger, err := zapConf.Build()
	if err != nil {
		return nil, err
	}

	return logger, nil
}
