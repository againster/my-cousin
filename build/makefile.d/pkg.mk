# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUGHOR: ArrowLee
# DATE: 20190920

include env.mk
include com.mk

rpmpkg_DATA = ${rpmdir}
srpmpkg_DATA = ${srpmdir}
rpmpkgdir = ${abs_top_deploy_pkgrpmdir}

rpmdir = rpm
srpmdir = srpm
moduledir = .
abs_moduledir = ${abs_builddir}
abs_module_rpmdir = ${abs_moduledir}/${rpmdir}
abs_module_srpmdir = ${abs_moduledir}/${srpmdir}
module_rpmdir = ${moduledir}/${rpmdir}
module_srpmdir = ${moduledir}/${srpmdir}
modulename = $(notdir ${abs_moduledir})

RPMBUILD = /usr/bin/rpmbuild --quiet --define='moduledir $(abs_moduledir)'
RPMBUILDBIN = $(RPMBUILD) --define='_rpmdir ${abs_module_rpmdir}' -bb
RPMBUILDSRC = $(RPMBUILD) --define='_srcrpmdir ${abs_module_srpmdir}' -bs

all: rpmbin

install: install-rpmbin

uninstall: uninstall-rpmbin

clean: clean-rpmbin

rpmbin:
	@list="$(rpm_SPECS)"; test -n "$(rpm_SPECS)" || exit 0; \
	test -d "$(moduledir)/rootfs" || exit 0; \
	for p in $$list; do \
		echo " $(RPMBUILDBIN) $(rpm_SPECS)"; \
		$(RPMBUILDBIN) $(rpm_SPECS) || exit $$?; \
	done;

rpmsrc:

clean-rpmbin:
	@test -d "$(module_rpmdir)" || exit 0; \
	echo " rm -rf $(module_rpmdir)"; rm -rf $(module_rpmdir);

clean-rpmsrc:
	@test -d "$(module_srpmdir)" || exit 0; \
	echo " rm -rf $(module_srpmdir)"; rm -rf $(module_srpmdir);

install-rpmbin: rpmbin
	@list="$(rpmpkg_DATA)"; test -n "$(rpmpkg_DATA)" || exit 0; \
	for p in $$list; do \
	  dst=$(rpmpkgdir); $(am__install_files_or_dirs); \
	done;

install-srpmbin: rpmsrc

uninstall-rpmbin:
	@list="$(rpmpkg_DATA)"; test -n "$(rpmpkg_DATA)" || exit 0; \
	for p in $$list; do \
	  dst=$(rpmpkgdir); $(am__uninstall_files_or_dirs); \
	done;

uninstall-srpmbin:
