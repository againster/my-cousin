package sdk

import (
	"mq"
	"protocol"
	"github.com/golang/protobuf/proto"
	"fmt"
)

type EmailSenderSdk struct {
	kafka *mq.KafkaWriter
	conf  *mq.KafkaWriterOptions
}

func NewEmailSenderSdk(options mq.KafkaWriterOptions) (*EmailSenderSdk, error) {
	writer, err := mq.NewKafkaWriter(options)
	sdk := &EmailSenderSdk{
		kafka: writer,
		conf: &options,
	}
	return sdk, err
}

func (p *EmailSenderSdk) marshal(emailAddr string, emailAddrAlias string,
	i18n string, smtpType string, captcha string, ttl uint64) ([]byte, error) {
	tplCaptcha := &protocol.EmailSenderTplCaptcha{
		To: &protocol.EmailRecipient{
			Alias: emailAddrAlias,
			Addr:  emailAddr,
		},
		Code: captcha,
		Ttl:  ttl,
	}
	c, err := proto.Marshal(tplCaptcha)
	if err != nil {
		return nil, fmt.Errorf("marshal tpl captcha error %s", err.Error())
	}
	param := &protocol.EmailSenderParam{
		Id:       0,
		I18N:     i18n,
		SmtpType: smtpType,
		TplType:  protocol.EmailSenderTplType_TplCaptcha,
		Param:    c,
	}
	s, err := proto.Marshal(param)
	if err != nil {
		return nil, fmt.Errorf("marshal tpl captcha error %s", err.Error())
	}
	return s, nil
}

func (p *EmailSenderSdk) Write(emailAddr string, emailAddrAlias string,
	i18n string, smtpType string, captcha string, ttl uint64) (int, error) {
	c, err := p.marshal(emailAddr, emailAddrAlias, i18n, smtpType, captcha, ttl)
	if err != nil {
		return 0, err
	}
	return p.kafka.Write(p.conf.Topic, c)
}

func (p *EmailSenderSdk) AsyncWrite(emailAddr string, emailAddrAlias string,
	i18n string, smtpType string, captcha string, ttl uint64) (int, error) {
	c, err := p.marshal(emailAddr, emailAddrAlias, i18n, smtpType, captcha, ttl)
	if err != nil {
		return 0, err
	}
	return p.kafka.AsyncWrite(p.conf.Topic, c)
}

func (p *EmailSenderSdk) AsyncReplyTo(f func(topic *string, payload []byte,
	err error)) {
	p.kafka.AsyncReplyTo(f)
}

func (p *EmailSenderSdk) Close() error {
	return p.kafka.Close()
}
